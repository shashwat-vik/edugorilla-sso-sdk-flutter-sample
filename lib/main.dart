import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';


void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(


        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});


  final String title;


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  static const platform =  MethodChannel('com.example.sdk_integration_demo/sso_sdk');

  String _responseFromNativeCode = 'Waiting for Response...';

  Future<void> responseFromNativeCode() async {
    String response = "";
    try {
      /**
       * user_name is the current user name example Abc;
       * user_email is the current user email id example abc@gmail.com.
       * user_mobile is the current user mobile number and it should not more than 10 digits example 9876543210.
       */
      final String result = await platform.invokeMethod("implementSsoSdk", {"user_name": "current user name", "user_email":"current user email", "user_mobile": "current user mobile number"});
      response = result;
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }
    setState(() {
      _responseFromNativeCode = response;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(


        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            InkWell(
              splashColor: Colors.green,
              highlightColor: Colors.blue,
              child: Text("Start SSO SDK"),
              onTap: () {
                setState(() {
                  responseFromNativeCode();
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
