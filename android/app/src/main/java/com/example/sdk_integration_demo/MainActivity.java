package com.example.sdk_integration_demo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.edugorilla.ssologin.EdugorillaSSO;

import org.json.JSONObject;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "com.example.sdk_integration_demo/sso_sdk";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
                .setMethodCallHandler(
                        (call, result) -> {
                            if (call.method.equals("helloFromNativeCode")) {
                                String message = "write a success message";
                                try {
                                    /**
                                     * Set base url in BASE_URL  and store the shared private key in raw file
                                     * BASE_URL means client portal URL. For example: "https://www.abc.com".
                                     * Package name means activity package name. For example: the current activity package name is com.example.sdk_integration_demo
                                     * written on the top of the activity
                                     */

                                    EdugorillaSSO.initializeBaseUrlAndFileLocation("BASE_URL", getResources().getIdentifier("private_key", "raw", "Package name"));
                                    JSONObject user_info = new JSONObject();
                                    user_info.put("name", call.argument<String>("user_name"))
                                    user_info.put("email", call.argument<String>("user_email"))
                                    user_info.put("mobile", call.argument<String>("user_mobile"))
                                    EdugorillaSSO.encryptUrlAndOpenWebView(this, user_info.toString(), "/");
                                } catch (Exception e) {
                                    Log.e("Exception", "" + e);
                                }
                                result.success(message);
                                return;
                            }
                            result.notImplemented();
                            return;
                        }
                );
    }

}
